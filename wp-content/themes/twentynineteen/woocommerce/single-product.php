<?php 

if (!defined('ABSPATH')) {
	exit; 
}

get_header();

global $product;

?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script>
$(function () {
    $('header').addClass('is_catalog');
});
</script>

<div id="content" class="content">
	<div class="container-fluid">
		<?php while(have_posts()) : the_post(); ?>
			
			<div class="row">
				<div class="col-12">
					<?php do_action('woocommerce_before_main_content');?>
				</div>
			</div>

			<div class="row mt-4">
				<div class="col-md-5">
					<div class="d-flex flex-column justify-content-between align-items-center">
						<?php the_title('<h1 class="product__title product__title_mobile">', '</h1>') ;?>
						<?php do_action('woocommerce_before_single_product_summary');?>
					</div>
				</div>

				<div class="col-md-7">
					<div class="product">
						<?php 
							the_title('<h1 class="product__title">', '</h1>');
							?><p class="cardbox__price"><?=$product->get_regular_price() . ' ₽'; ?></p>
							<div class="cardbox__excerpt">
								<?php the_excerpt();?>
								<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div>
							</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="tab-box">
						<button class="tab-box__tab tab-box__tab_active" data-tab="1">Описание</button>
						<button class="tab-box__tab" data-tab="2">Видео</button>
						<button class="tab-box__tab" data-tab="3">Каталог</button>
					</div>

					<div class="tab-content">
						<div class="tab-content__tab tab-content__tab_active" data-tab="1">
							<?php the_content(); ?>
						</div>

						<div class="tab-content__tab" data-tab="2">
							<div class="videobox">
								<?php do_action('get_product_video'); ?>
							</div>
						</div>

						<div class="tab-content__tab" data-tab="3">
							<a class="tab__catalog" href="<?=do_shortcode('[userCatalog]'); ?>" download>
								<img src="<?=get_template_directory_uri();?>/assets/images/main/link.webp" alt="link"> Скачать каталог
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
	</div>
</div>

<?php get_footer(); ?>