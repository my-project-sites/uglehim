<?php

if (!defined('ABSPATH')) {
	exit;
}

$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);

?>

<div class="cardbox__card">
	<a href="<?=get_term_link($category);?>">
		<img class="cardbox__image" src="<?=wp_get_attachment_url($thumbnail_id);?>" alt="img">
	</a>
	<a class="cardbox__title" href="<?=get_term_link($category);?>">
		<h3><?=$category->name;?></h3>
	</a>
	<a class="cardbox__link" href="<?=get_term_link($category);?>">Выбрать</a>
</div>