<?php

if (!defined('ABSPATH')) {
	exit;
}

if ($related_products) : ?>
	<h2 class="m-header-related"><?php esc_html_e( 'Related products', 'woocommerce' ); ?></h2>
	<div class="d-flex flex-row justify-content-center align-items-center">
		<?php foreach ( $related_products as $related_product ) : ?>
			<?php

				$post_object = get_post( $related_product->get_id() );
				setup_postdata( $GLOBALS['post'] =& $post_object );
				wc_get_template_part( 'content', 'product' ); 
			?>
		<?php endforeach; ?>
	</div>
<?php endif;

wp_reset_postdata();