<footer>
	<a href="#header" id="link-scroll">Вверх</a>
	<div class="footerbox">
		<ul class="footerbox__menu">
			<li class="footerbox__item">Главная
				<ul class="footerbox__submenu">
					<li>
						<a class="footerbox__sublink" href="<?php echo get_home_url(); ?>">
							На Главную
						</a>
					</li>
				</ul>
			</li>
			<li class="footerbox__item">Каталог
				<?php
					wp_nav_menu( [
						'theme_location'  => 'bottom', 
						'container'       => false, 
						'container_class' => '', 
						'container_id'    => '',
						'menu_class'      => 'footerbox__submenu', 
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => '',
					]);
				?>
			</li>
			<li class="footerbox__item">Статьи
				<ul class="footerbox__submenu">
					<li>
						<a class="footerbox__sublink" href="/stati/">Читать</a>
					</li>
				</ul>
			</li>
			<li class="footerbox__item">О нас
				<ul class="footerbox__submenu">
					<li>
						<a class="footerbox__sublink" href="/o-kompanii/">Узнать больше</a>
					</li>
				</ul>
			</li>
			<li class="footerbox__item">Контакты
				<ul class="footerbox__submenu">
					<li>
						<a class="footerbox__sublink" href="/kontakty/">Связаться с нами</a>
					</li>
				</ul>
			</li>
		</ul>

		<div class="footerbox__info">
			<a href="<?php echo get_home_url();?>">
				<img src="<?=get_template_directory_uri();?>/assets/images/main/logo.webp" alt="logo">
			</a>

			<div class="footerbox__subscribe">
				<?=do_shortcode('[mailpoet_form id="2"]'); ?>
			</div>

			<div class="footerbox__slogan">
				<p>Подписываясь на нашу рассылку, вы выражаете согласие с нашей <a class="footerbox__private" href="/politika-konfidenczialnosti/">политикой конфиденциальности.</a></p>
			</div>

			<p class="footerbox__design">DEVELOP BY</p>
			<div class="footerbox__developer">
				<a target="_blank" href="https://www.facebook.com/bloodborne.gothic">
					<img class="footerbox__dev-logo" src="<?=get_template_directory_uri();?>/assets/images/main/my-logo.webp" alt="logo">
				</a>
			</div>
		</div>	
	</div>
	<hr class="footerbox__line">
	<p class="footerbox__copyright">© 1994 — <?=date('Y');?> ООО "Углехимпродукт". Все права защищены. Любая информация, содержащаяся на данном сайте, имеет справочный характер и не является публичной офертой, предусмотренной ст. 437 Гражданского Кодекса РФ. Цены указаны на сайте исключительно для ознакомления и могут отличаться от цен в магазине.</p>
</footer>

<?php wp_footer(); ?>

<!-- Форма поиска -->
<div class="modal" id="searchModal" tabindex="-1" role="dialog">
	<?php echo do_shortcode('[smart_search id="2"]'); ?>
</div>

</div> <!-- End wrapper -->

</body>
</html>