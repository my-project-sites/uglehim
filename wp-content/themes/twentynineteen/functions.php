<?php

/*-------------------*/
/***** WORDPRESS *****/
/*-------------------*/

# Шорткод для home_url
add_shortcode('main_url', 'url_func');
function url_func() {
    return get_home_url();
}

# Шорткод для site_name
add_shortcode('site_name', 'site_name');
function site_name() {
    $name = get_home_url();
    $number = stripos($name, '//');
    return 'www.' . substr($name, $number + 2);
}

# Отладочная функция
function out($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

# Вывод header_text
function get_header_text($post_id, $category_id) {
	if (is_page(132)) {
		echo '<h1 class="write-up animation">' . get_post_meta($post_id, 'slogan', true) . '</h1>';
	}
	if(is_tax()) {
		echo '<p class="write-up animation">' . category_description($category_id) . '</p>';
	}
	elseif(is_home()) {
		echo '<p class="write-up animation">' . 'В наших статьях вы можете найти различную полезную информацию.' . '</p>';
	}
	else {
		echo '<p class="write-up animation">' . get_post_meta($post_id, 'slogan', true) . '</p>';
	}
}

# Подключаем стили в шапке
add_action('wp_enqueue_scripts', 'header_styles');
function header_styles() {
	wp_enqueue_style('Open+Sans', get_template_directory_uri() . '/assets/fonts/open+sans.css');
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
}

# Подключаем стили в футере
add_action('wp_footer', 'footer_styles');
function footer_styles() {
	wp_enqueue_style('style', get_stylesheet_uri());
}

# Подключаем скрипты в шапке
add_action('wp_enqueue_scripts', 'header_scripts');
function header_scripts() {
	wp_deregister_script('jquery');
	wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery-3.4.1.min.js');
	wp_enqueue_script('jquery');
}

# Подключаем скрипты в шапке
add_action('wp_footer', 'footer_scripts');
function footer_scripts() {
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	wp_enqueue_script('es5-shims.min.js', get_template_directory_uri() . '/assets/js/es5-shims.min.js');
	wp_enqueue_script('share.js', get_template_directory_uri() . '/assets/js/share.js');
	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js');

	# Категории
	if(is_tax()) {
		wp_enqueue_script('catalog', get_template_directory_uri() . '/assets/js/catalog.js');
	}
	# Статьи
	if(is_home()) {
		wp_enqueue_script('articles', get_template_directory_uri() . '/assets/js/articles.js');
	}
	# О компании
	if(is_page(96)) { 
		wp_enqueue_script('company', get_template_directory_uri() . '/assets/js/company.js');
	}
	# Контакты
	if(is_page(98)) {
		wp_enqueue_script('contacts', get_template_directory_uri() . '/assets/js/contacts.js');
	}
	# Консультирование
	if(is_page(180)) {
		wp_enqueue_script('consultation', get_template_directory_uri() . '/assets/js/consultation.js');
	}
	# Продажа
	if(is_page(182)) {
		wp_enqueue_script('sale', get_template_directory_uri() . '/assets/js/sale.js');
	}
	# Монтаж
	if(is_page(184)) {
		wp_enqueue_script('montage', get_template_directory_uri() . '/assets/js/montage.js');
	}
	# Проектирование
	if(is_page(186)) {
		wp_enqueue_script('sale', get_template_directory_uri() . '/assets/js/project.js');
	}
	# 404
	if(is_404()) {
		wp_enqueue_script('404', get_template_directory_uri() . '/assets/js/404.js');
	}
}

# Регистрируем меню
register_nav_menus(array(
	'top'    => 'Главное меню',
	'bottom' => 'Категории'
));

# Регистрируем функцию, получающую выдержку
function custom_excerpt($text) {
	echo substr($text, 0, stripos($text, ' ', 100)) . '...';
}

# Удалим эмоции
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

# Отключим шрифты у Elementor
add_filter('elementor/frontend/print_google_fonts', '__return_false');

# Отключаем Гутенберг
if('disable_gutenberg') {
	add_filter('use_block_editor_for_post_type', '__return_false', 100);
	remove_action('wp_enqueue_scripts', 'wp_common_block_scripts_and_styles');
	add_action('admin_init', function(){
		remove_action( 'admin_notices', ['WP_Privacy_Policy_Content', 'notice'] );
		add_action( 'edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
	});
}

# Удалить атрибут type у styles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}

# Удалить атрибут type у scripts 
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

# Асинхронная загрузка для скриптов, подключенных через wp_enqueue_script
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function add_async_attribute($tag, $handle) {
	if(!is_admin()) {
	    if ('jquery-core' == $handle) {
	        return $tag;
	    }
	    return str_replace(' src', ' defer src', $tag);
	}
	else {
		return $tag;
	}
}

# Подключение стилей в админке
add_action('admin_head', 'wph_inline_css_admin');
function wph_inline_css_admin() {
echo '<style>

.plugin-title span a[href="http://www.never5.com/?utm_source=plugin&utm_medium=link&utm_campaign=what-the-file"] {
	display: none !important;
}

#adminmenuback, #adminmenuwrap, #adminmenu {
	background-color: #23282d !important;
}

.wramvp_img_email_image,
#wp-admin-bar-view-store, 
#wp-admin-bar-view-site,
.aioseop_help_text_link, 
.upgrade_menu_link, 
#aio-pro-update, 
.proupgrade, .notice-error, .column-RV,
.column-MV, .woocommerce-help-tip,
.updated, #dashboard_php_nag,
.mailpoet-dismissible-notice,
.skiptranslate,
.mailpoet_feature_announcement,
.elementor-plugins-gopro,
.aioseop-notice-woocommerce_detected,
.aioseop_nopad_all,
.help_tip, .aio_orange_box,
.wbcr-upm-plugin-status 
{
	display: none !important;
}

#thumb {
	color: transparent;
}

</style>';
}

/*---------------------*/
/***** WOOCOMMERCE *****/
/*---------------------*/

# Отключим стили woocommerce
add_filter('woocommerce_enqueue_styles', '__return_empty_array');
# Отключим рейтинг
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
# Удалим обертку
remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
# Удалим блок с оповещениями
remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10);
# Удалим отображение числа товаров
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

# Настроим хлебные крошки
add_filter('woocommerce_breadcrumb_defaults','custom_breadcrumb');
function custom_breadcrumb() {
	return array(
		'delimiter'   => ' \ ',
		'wrap_before' => '<div class="woocommerce-breadcrumb">',
		'wrap_after'  => '</div>',
		'before'      => '',
		'after'       => '',
		'home'        => _x('Home', 'breadcrumb', 'woocommerce')
	);
}

# Число товаров на одной странице
add_filter('loop_shop_per_page', function ($cols) {
    return 6;
}, 20);

add_filter( 'woocommerce_product_subcategories_hide_empty', function() { return false; }, 10, 1 );

add_filter('woocommerce_get_image_size_thumbnail','add_thumbnail_size',1,10);
function add_thumbnail_size($size){

    $size['width'] = 200;
    $size['height'] = 200;
    $size['crop']   = 0; # 0 - не обрезаем, 1 - обрезка
    return $size;
}

# Видеоролики
add_action('woocommerce_product_options_pricing', 'create_video_field');
function create_video_field() {
?>  <div class="options_group">
		<?php
			woocommerce_wp_text_input(array(
				'id'                => 'video_one',
				'label'             => 'Видеоролик № 1', 'woocommerce',
				'placeholder'       => '',
				'desc_tip'          => false
			));

			woocommerce_wp_text_input(array(
				'id'                => 'video_two',
				'label'             => 'Видеоролик № 2', 'woocommerce',
				'placeholder'       => '',
				'desc_tip'          => false
			));

			woocommerce_wp_text_input(array(
				'id'                => 'video_three',
				'label'             => 'Видеоролик № 3', 'woocommerce',
				'placeholder'       => '',
				'desc_tip'          => false
			));
		?>
	</div><?php
};

add_action('woocommerce_process_product_meta', function($post_id) {

	$text_field = isset($_POST['video_one']) ? sanitize_text_field($_POST['video_one']) : '';
	update_post_meta($post_id, 'video_one', esc_attr($text_field));

	$text_field = isset($_POST['video_two']) ? sanitize_text_field($_POST['video_two']) : '';
	update_post_meta($post_id, 'video_two', esc_attr($text_field));

	$text_field = isset($_POST['video_three']) ? sanitize_text_field($_POST['video_three']) : '';
	update_post_meta($post_id, 'video_three', esc_attr($text_field));
}, 10, 1);

add_action('get_product_video', function() {

	$product = wc_get_product();
	$path_1 = get_post_meta($product->get_id(), 'video_one', true);
	$path_2 = get_post_meta($product->get_id(), 'video_two', true);
	$path_3 = get_post_meta($product->get_id(), 'video_three', true);

	$movies = [$path_1, $path_2, $path_3];

	foreach($movies as $key => $value) {
		
		if($value != '') {
			echo '<iframe class="videobox__video" src="https://www.youtube.com/embed/' . $value . '"' . 'frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';			
		}
	}

	if($movies[0] == '' & $movies[1] == '' & $movies[2] == '') {
		echo '<p>' . 'Для данного товара видео отсутствует.' .'</p>';
	}
});

# Мета-поля для категорий товаров
add_action("product_cat_edit_form_fields", 'mayak_meta_product_cat');
function mayak_meta_product_cat($term){
    ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label>Заголовок (title)</label></th>
            <td>
                <input type="text" name="mayak[title]" value="<?php echo esc_attr( get_term_meta( $term->term_id, 'title', 1 ) ) ?>"><br />
                <p class="description">Не более 60 знаков, включая пробелы</p>
            </td>
        </tr>
    <?php
}

# Сохранение данных в БД
add_action('edited_product_cat', 'mayak_save_meta_product_cat'); 
add_action('create_product_cat', 'mayak_save_meta_product_cat');
function mayak_save_meta_product_cat($term_id){
    if (!isset($_POST['mayak']))
        return;
    $mayak = array_map('trim', $_POST['mayak']);
    foreach($mayak as $key => $value){
        if(empty($value)){
            delete_term_meta($term_id, $key);
            continue;
        }
        update_term_meta($term_id, $key, $value);
    }
    return $term_id;
}

# Вывод title для категорий товаров
add_filter('single_term_title', 'mayak_filter_single_cat_title', 10, 1);
add_filter( 'single_term_title', 'mayak_poduct_cat_title', 10, 1);
function mayak_filter_single_cat_title() {
    $pci =  get_queried_object()->term_id;
    return get_term_meta ($pci, 'title', true);
}
function mayak_poduct_cat_title($pct){
    if(empty($pct)){
        $pct = get_queried_object()->name;
    }
    return $pct;
}

# Очистим описание категорий от тегов
add_filter('term_description', 'custom_clear_term_description');
function custom_clear_term_description($value){
	$value = str_replace('<p>', '', $value);
	return str_replace('</p>', '', $value);
}

?>