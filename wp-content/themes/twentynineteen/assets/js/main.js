jQuery(document).ready(function ($) {

    // Зададим домашнюю страницу
    const HOME_URL = 'https://uglehim.xyz';

    // Форма поиска
    $('.topmenu__loupe').click(function(){
        $('#searchModal').modal('show');
    });

    // Табы на вкладке товара
    $('.tab-box__tab').on('click', function() {
        $('.tab-box__tab').removeClass('tab-box__tab_active');
        $(this).toggleClass('tab-box__tab_active');
        $('.tab-content__tab').removeClass('tab-content__tab_active');
        $('.tab-content__tab[data-tab=' + $(this).attr('data-tab') + ']').toggleClass('tab-content__tab_active');
    })

    // Откроем адаптивное меню
    $('.topmenu__button').click(function() {
        $('.topmenu__list').toggleClass('topmenu__list_active');
    })

    // Плавный скролл
    $('#link-scroll').on('click', function(event){
        event.preventDefault();
        var target = $(this).attr('href');
        var top = $(target).offset().top;
        $('html, body').animate({scrollTop: top}, 600);
    });
    
    // Переход по ссылкам каталога на 2-й экран
    $('#primary a, .cardbox a, .page, .previouspostslink, .nextpostslink').on('click', function() {
        // Отменим стандартное действие у ссылок
        event.preventDefault();
        
        // Переадресуем пользователя при клике
        var path = $(this).attr('href');
        if(path == HOME_URL) {
            document.location.href= HOME_URL;
        }
        else {
            var path = path + '#content';
            document.location.href= path;
        }
    });

    // Добавим классы в меню
     $('.footerbox__submenu a').addClass('footerbox__sublink');

    // Добавим placeholder в форму подписки
    $('.footerbox__subscribe input[type="email"]').attr("placeholder", "ВВЕДИТЕ ВАШ EMAIL");

});