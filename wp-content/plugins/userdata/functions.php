<?php

/* Активация */
function AT_install() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    if($wpdb->get_var("SHOW TABLES LIKE $table") != $table) {
        $sql = "CREATE TABLE IF NOT EXISTS $table (catalog text, email text, address text, phone text, phone2 text, phone3 text, chart text, vk text, facebook text, youtube text, instagram text, telegram text, behance text, twitter text) ENGINE=MyISAM DEFAULT CHARSET=utf8";
        $wpdb->query($sql);

        $into = "INSERT INTO $table (catalog, email, address, phone, phone2, phone3, chart, vk, facebook, youtube, instagram, telegram, behance, twitter) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";
        $sql = $wpdb->prepare($into, '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        $wpdb->query($sql);
    }
}

/* Деактивация */
function AT_deinstall() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $sql = "DROP TABLE IF EXISTS $table";
    $wpdb->query($sql);
}

/* Удаление */
function AT_uninstall() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $sql = "DROP TABLE IF EXISTS $table";
    $wpdb->query($sql);
}

/* Добавление раздела в admin панель */
function AT_addSection() {
    add_menu_page('Контактные данные', 'Контакты', 7, 'network', 'AT_backEnd', 'dashicons-facebook', '2.1');
}

function AT_backEnd() {
    include 'backend/controller.php';
    include 'backend/view.php';
}

/* Получим значения */
function AT_getData() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data;
}

/* Внесем значения  */  
function AT_addData($catalog, $email, $address, $phone, $phone2, $phone3, $chart, $vk, $facebook, $youtube, $instagram, $telegram, $behance, $twitter) {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $update = "UPDATE $table SET catalog='%s', email='%s', address='%s', phone='%s', phone2='%s', phone3='%s', chart='%s', vk='%s', facebook='%s', youtube='%s', instagram='%s', telegram='%s', behance='%s', twitter='%s' ";
    $sql = $wpdb->prepare($update, $catalog, $email, $address, $phone, $phone2, $phone3, $chart, $vk, $facebook, $youtube, $instagram, $telegram, $behance, $twitter);
    $result = $wpdb->query($sql);

    if($result === false) {
        echo '<p class="AT_danger">' . 'Ошибка БД' . '</p>';
        die('');
    }

    return true;
}

/* Получим email пользователя */
function AT_email() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['email'];
}

/* Получим телефон пользователя */
function AT_phone() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['phone'];
}

/* Получим телефон пользователя № 2 */
function AT_phone2() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['phone2'];
}

/* Получим телефон пользователя № 3*/
function AT_phone3() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['phone3'];
}

/* Получим телефон пользователя */
function AT_chart() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['chart'];
}

/* Получим адрес пользователя */
function AT_address() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['address'];
}

/* Получим VK */
function AT_vk() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['vk'];
}

/* Получим facebook */
function AT_facebook() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['facebook'];
}

/* Получим youtube */
function AT_youtube() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['youtube'];
}

/* Получим instagram */
function AT_instagram() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['instagram'];
}

/* Получим telegram */
function AT_telegram() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['telegram'];
}

/* Получим behance */
function AT_behance() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['behance'];
}

/* Получим twitter */
function AT_twitter() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['twitter'];
}

/* Получим cataloog */
function AT_catalog() {
    global $wpdb;
    $table = $wpdb->prefix . 'userdata';
    $query = "SELECT * FROM $table";
    $data = $wpdb->get_row($query, ARRAY_A);
    return $data['catalog'];
}

?>