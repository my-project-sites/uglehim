<?php 
	get_header(); 
	wp_reset_postdata();
?>

<?php the_content(); ?>

<div class="servicebox">
	<div>
		<div class="servicebox__advice">
			<a class="servicebox__link servicebox__link_advice" href="/konsultirovanie">Консультирование</a>
			<p class="servicebox__text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit sapiente eveniet, quod id vero cumque qui accusamus provident ea recusandae! Perferendis ipsam, vel ullam eum numquam nesciunt ipsum incidunt consequuntur!</p>
		</div>
		<div class="servicebox__project">
			<a class="servicebox__link servicebox__link_project" href="/proektirovanie">Проектирование</a>
			<p class="servicebox__text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit sapiente eveniet, quod id vero cumque qui accusamus provident ea recusandae! Perferendis ipsam, vel ullam eum numquam nesciunt ipsum incidunt consequuntur!</p>
		</div>
	</div>

	<div>
		<div class="servicebox__sale">
			<a class="servicebox__link servicebox__link_sale" href="/prodazha-materialov">Продажа материалов</a>
			<p class="servicebox__text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit sapiente eveniet, quod id vero cumque qui accusamus provident ea recusandae! Perferendis ipsam, vel ullam eum numquam nesciunt ipsum incidunt consequuntur!</p>
		</div>
		<div class="servicebox__montage">
			<a class="servicebox__link servicebox__link_montage" href="/montazh-sistem">Монтаж систем</a>
			<p class="servicebox__text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit sapiente eveniet, quod id vero cumque qui accusamus provident ea recusandae! Perferendis ipsam, vel ullam eum numquam nesciunt ipsum incidunt consequuntur!</p>
		</div>
	</div>
</div>

<div class="clientbox">
<h2>Нам доверяют:</h2>
	<div class="clientbox__mobile">
		<img class="clientbox__image" src="<?=get_template_directory_uri();?>/assets/images/clients/clients_mobile.webp" alt="img">
	</div>
	<div class="clientbox__desktop">
		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/1.webp" alt="img">
			<p class="clientbox__title">Администрация Калининграда</p>
		</div>

		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/2.webp" alt="img">
		</div>

		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/3.webp" alt="img">
		</div>

		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/4.webp" alt="img">
		</div>

		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/5.webp" alt="img">
		</div>

		<div class="clientbox__poster">
			<img src="<?=get_template_directory_uri();?>/assets/images/clients/6.webp" alt="img">
		</div>
	</div>
</div>

<?php 
	include_once 'templates/partnerbox.php';
	get_footer(); 
?>