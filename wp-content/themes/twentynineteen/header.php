<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="yandex-verification" content="963f0670179da096">
	
	<title></title>	
	<?php wp_head(); ?>
</head>
<body>
<div id="wrapper">
<header id="header">
	<div class="topmenu">
		<a class="topmenu__logo" href="<?php echo get_home_url();?>">
			<img src="<?=get_template_directory_uri();?>/assets/images/main/logo.webp" alt="logo">
		</a>
	
		<?php
			wp_nav_menu( [
				'theme_location'  => 'top',
				'menu'            => '', 
				'container'       => false, 
				'container_class' => '', 
				'container_id'    => '',
				'menu_class'      => 'topmenu__list', 
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => '',
			]);
		?>
	
		<img class="topmenu__loupe" src="<?=get_template_directory_uri();?>/assets/images/main/search.webp" alt="search">

		<img class="topmenu__button" src="<?=get_template_directory_uri();?>/assets/images/main/button.webp" alt="button">

		<?php 
			$post_id = $post->ID;
			$category_id = get_queried_object()->term_id;
			get_header_text($post_id, $category_id); 
		?>
	</div>
</header>