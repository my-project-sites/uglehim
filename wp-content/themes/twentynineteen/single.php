<?php 
	get_header();
	wp_reset_postdata(); 
?>

<div id="content" class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title();?></h1>
				<div>
					<?php the_content();?>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="d-flex flex-row justify-content-center align-items-center">
					<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>