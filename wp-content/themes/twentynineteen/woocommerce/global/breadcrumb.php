<?php

if (!defined('ABSPATH')) {
	exit;
}

if (!empty($breadcrumb)) {

	echo $wrap_before;

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if (!empty( $crumb[1]) && sizeof($breadcrumb) !== $key + 1) {
			echo '<a class="breadcrumb-box__link" href="' . esc_url( $crumb[1] ) . '">' . esc_html( $crumb[0] ) . '</a>';
		} 
		else {
			echo '<span class="breadcrumb-box__item">' . $crumb[0] . '</span>';
			# Создадим глобальную переменную с названиеммм категории
			$GLOBALS['my_category_name'] = $crumb[0];
		}

		echo $after;

		if (sizeof($breadcrumb) !== $key + 1) {
			echo $delimiter;
		}
	}

	echo $wrap_after;

}