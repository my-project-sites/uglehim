<?php

if (!defined('ABSPATH')) {
	exit; 
}

get_header();

?>

<div id="content" class="content">

	<div class="breadcrumb-box">
		<?php 
			# Хлебные крошки
			do_action('woocommerce_before_main_content'); 
		?>
	</div>

	<?php 
	

	if (have_posts()) :
		
		?><h1><?=$my_category_name; ?></h1><?
		# Вывод подкатегорий
		woocommerce_product_loop_start();
		woocommerce_product_loop_end();

		# Вывод товаров
		?><div class="cardbox"><?php
			while(have_posts()) : the_post();
				wc_get_template_part('content', 'product-cat');
			endwhile; 
		?></div><?php
		
		# Постраничная навигация
		wp_pagenavi();

		else: echo '<h2>' . 'Страница находится на стадии заполнения...' . '</h2>';

	endif;

	?>
</div>

<?php get_footer(); ?>