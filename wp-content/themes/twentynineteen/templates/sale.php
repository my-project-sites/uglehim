<?php 
	/* Template Name: Продажа */
	get_header(); 
	wp_reset_postdata();
?>

<div class="container-fluid mb-5">
	<div class="row">
		<div class="col-md-12">
			<h1>Продажа материалов для систем отопления</h1>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<p class="font-weight-bold mt-4">Мы продаем только качественные товары от лучших европейских производителей.</p>

			<ul class="content__list">
				<li class="content__item">Радиаторы</li>
				<li class="content__item">Термоголовки</li>
				<li class="content__item">Насосы</li>
				<li class="content__item">Распределители</li>
				<li class="content__item">Трубы и изоляция для труб</li>
				<li class="content__item">Расширительные баки</li>
				<li class="content__item">Конвекторы</li>
				<li class="content__item">Фильтры для воды</li>
				<li class="content__item">Регуляторы температуры</li>
				<li class="content__item">Фитинги</li>
				<li class="content__item">Краны шаровые</li>
				<li class="content__item">Обратные клапана</li>
				<li class="content__item">Фильтры грубой очистки</li>
				<li class="content__item">Группы безопасности и комплектующие</li>	
			</ul>
		</div>

		<div class="col-12">
			<? include 'partnerbox.php'; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>