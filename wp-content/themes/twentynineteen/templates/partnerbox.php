<div class="partnerbox">
	<h2>Наши партнеры:</h2>
	<div>
		<div class="mt-5 d-flex flex-row justify-content-around align-items-center">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/henco.webp" alt="henco">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/dab.webp" alt="dab">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/itap.webp" alt="itap">
		</div>

		<div class="mt-5 d-flex flex-row justify-content-around align-items-center">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/wavin.webp" alt="wavin">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/caleffi.webp" alt="caleffi">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/techniprot.webp" alt="techniprot">
		</div>

		<div class="mt-5 d-flex flex-row justify-content-around align-items-center">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/heaton.webp" alt="heaton">
			<img class="partnerbox__image" src="<?=get_template_directory_uri();?>/assets/images/partners/isoterm.webp" alt="isoterm">
		</div>
	</div>
</div>