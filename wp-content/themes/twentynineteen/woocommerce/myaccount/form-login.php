<?php

if (!defined('ABSPATH')) {
	exit;
}

do_action( 'woocommerce_before_customer_login_form' ); ?>

<!-- Форма входа в аккаунт -->
<div class="row">
	<div class="col-md-12">
		<form class="woocommerce-form" method="post">
			<?php do_action('woocommerce_login_form_start');?>
				<label for="username">
					<?php esc_html_e('Username or email address', 'woocommerce'); ?>&nbsp;<span class="required">*</span>
				</label>
				<input type="text" name="username" id="username" autocomplete="username" value="<?php echo (!empty( $_POST['username'])) ? esc_attr(wp_unslash($_POST['username'])) : ''; ?>">

				<label for="password"><?php esc_html_e('Password', 'woocommerce'); ?>&nbsp;<span class="required">*</span></label>
				<input type="password" name="password" id="password" autocomplete="current-password">

			<?php do_action('woocommerce_login_form'); ?>

			<div class="d-flex flex-column justify-content-start align-items-start">
				<label>
					<input name="rememberme" type="checkbox" id="rememberme" value="forever"> <span class="rememberMe"><?php esc_html_e('Remember me', 'woocommerce'); ?></span><br><br>
				</label>
				<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>
				<input type="submit" name="login" value="Войти">	
			</div>
			<p class="woocommerce-LostPassword lost_password">
				<!-- <a href="<?php echo esc_url(wp_lostpassword_url()); ?>">
					<?php esc_html_e('Lost your password?', 'woocommerce'); ?>
				</a> -->
			</p>
			<?php do_action('woocommerce_login_form_end'); ?>
		</form>
	</div>
</div>
<!-- END -->
		
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>