<?php

if (!defined('ABSPATH')) {
	exit; 
}

global $product;

if(empty($product) || !$product->is_visible()) {
    return;
}

# Получим ссылку на товар
$link = get_permalink($product->ID);
# Краткое описание
$text = $product->post->post_excerpt;
# Получим цену
$price = $product->get_regular_price();

?>

<div class="cardbox__card">
    <a href="<?=$link;?>">
        <?php do_action('woocommerce_before_shop_loop_item_title'); ?>
    </a>
    <a class="cardbox__title" href="<?=$link;?>"><?=get_the_title(); ?></a>
    <p class="cardbox__price"><?=$price . ' ₽'; ?></p>
    <a class="cardbox__link" href="<?=$link;?>">Выбрать</a>
</div>