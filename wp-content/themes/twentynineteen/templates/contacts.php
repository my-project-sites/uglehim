<?php 
    /* Template Name: Контакты */
	get_header(); 
	wp_reset_postdata();
?>

<div id="content" class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Вы можете связаться с нами любым удобным способом:</h1>
                <div class="contactbox">
                    <div>
                        <h3 class="contactbox__title">Адрес:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userAddress]'); ?></p>

                        <h3 class="contactbox__title">Почтовый ящик:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userEmail]'); ?></p>

                        <h3 class="contactbox__title">График работы:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userChart]'); ?></p>
                    </div>

                    <div>
                        <h3 class="contactbox__title">Офис:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userPhone]'); ?></p>

                        <h3 class="contactbox__title">Отдел продаж:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userPhone2]'); ?></p>
                        
                        <h3 class="contactbox__title">Главный инженер:</h3>
                        <p class="contactbox__text"><?=do_shortcode('[userPhone3]'); ?></p>
                    </div>
                </div> <!-- End contactbox -->
            </div>
        </div>
    </div>

    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="gis-map"> 
                    <div id="map" style="width:100%; height:100%"></div>
                </div>
                
                <script>        
                    var map;
                    DG.then(function () {
                        map = DG.map('map', {
                            center: [54.712425, 20.597794],
                            zoom: 13,
                            scrollWheelZoom: false
                        });

                        DG.marker([54.712425, 20.5979999]).addTo(map).bindPopup('ООО «УГЛЕХИМПРОДУКТ»');
                    });  
                </script>
            </div>
        </div>
    </div>

    <?php include_once 'social.php'; ?>
</div>

<?php get_footer(); ?>