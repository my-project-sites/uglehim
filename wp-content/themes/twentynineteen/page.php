<?php 
	get_header(); 
	wp_reset_postdata();
?>

<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div id="content" class="content">
				<h1><?=the_title(); ?></h1>
				<?php 
					the_content();

					include_once 'templates/social.php';
					
					if(is_page(96) || is_page(98)) {
						include_once 'templates/partnerbox.php';
					}	
				?>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>