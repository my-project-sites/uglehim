<?php
if (!defined('ABSPATH')) {
	exit;
}

global $post, $product;

if(has_post_thumbnail()) {
	
	$props = wc_get_product_attachment_props(get_post_thumbnail_id(), $post);
	$image = get_the_post_thumbnail( $post->ID, apply_filters('single_product_large_thumbnail_size', 'shop_single'), array(
		'class'	 => 'img-responsive',
		'alt'    => $props['alt'],
		'data-imagezoom' => "true"
	));
	
	echo apply_filters('woocommerce_single_product_image_html', 
		sprintf('<img class="cardbox__image" src="%s">', esc_url($props['url']), $image), $post->ID);		
}

?>