<?php

if (!defined('ABSPATH')) {
	exit;
}

// My Account dashboard.
do_action( 'woocommerce_account_dashboard' );

// Deprecated woocommerce_before_my_account action.
do_action( 'woocommerce_before_my_account' );

// Deprecated woocommerce_after_my_account action.
do_action( 'woocommerce_after_my_account' );