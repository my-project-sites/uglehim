<?php 
	/* Template Name: Монтаж систем */
	get_header(); 
	wp_reset_postdata();
?>

<div class="container-fluid mb-5">
	<div class="row">
		<div class="col-md-12">
			<h1>Проектирование систем отопления</h1>
			<p>
				Нужно смонтировать отопление в частном доме, квартире или промышленном здании? Перекладывайте все задачи на компанию "Углехим-Продукт" – сделаем отопление под ключ, от проекта до запуска!
			</p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<p class="font-weight-bold mt-4">Как мы монтируем отопление?</p>

			<ul class="content__list mt-2">
				<li class="content__item">Оцениваем объект.</li>
				<li class="content__item">Разрабатываем проект и план работ.</li>
				<li class="content__item">Подготавливаем проектно-сметную документацию при необходимости.</li>
				<li class="content__item">Доставляем на объект материалы и оборудование.</li>
				<li class="content__item">
					Монтируем систему. Иногда на этом этапе нужны согласования со смежными строительными работами или другими подрядчиками.
				</li>
				<li class="content__item">Запускаем систему и отлаживаем до стабильной работы</li>
				<li class="content__item">Выдаем гарантийный сертификат</li>
			</ul>

			<p class="mt-4">
				В нашем предложении вы увидите точную цену услуги: она не изменится впоследствии и не обрастет различными доплатами за «штробовку», «подрезку», «выравниваение» и прочие фантазии, которыми славятся некоторые строители.
			</p>

			<p class="mt-4">
				Сроки разработки плана и монтажа индивидуальные. Как правило, простые объекты мы проектируем за 3-4 дня, а монтаж системы отопления среднего частного дома занимает 1-2 недели.
			</p>

			<p class="mt-4">Поэтому точные стоимость и сроки мы называем после оценки и разработки проекта.</p>

			<p class="font-weight-bold mt-4">Углехим-Продукт – почему с нами удобно сотрудничать?</p>

			<ul class="content__list mt-2">
				<li class="content__item">
					<b>Качествення продукция</b> — поставляем трубы, радиаторы и другие элементы напрямую от производителей , которые работают по мировым стандартам качества.
				</li>
				<li class="content__item">
					<b>Возможность выполнить любой заказ</b> –  мы выбирали производителей систем отопления таким образом, чтобы удовлетворить любого клиента и выполнить нестандартные или специфические заказы.
				</li>
				<li class="content__item">
					<b>Гарантия и техническая поддержка</b> – оформляем гарантию на продукцию и монтаж отопления частного дома на 10 лет, в течение этого срока мы исправляем поломки или сбои в системе.
				</li>

			</ul>
		</div>
	</div>
</div>

<?php get_footer(); ?>