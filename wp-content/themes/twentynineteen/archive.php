<?php get_header(); ?>

<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-2">
			<?php
				wp_nav_menu( [
					'theme_location'  => 'articles',
					'menu'            => '', 
					'container'       => false, 
					'container_class' => '', 
					'container_id'    => '',
					'menu_class'      => 'category', 
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => '',
				]);
			?>
		</div>
		<div class="col-md-10">
			<div class="row">
				<?php while(have_posts()): the_post();?>	
					<div class="col-md-4">
						<div class="post">
							<h3 class="post-title"><?php the_title();?></h3>
							<?php the_post_thumbnail('medium');?>

							<div class="post-mask">
								<h3 class="post-title"><?php the_title();?></h3>
								<p class="post-text"><?php custom_excerpt(strip_tags(get_the_content()));?></p>
								<a class="post-link btn btn-primary" href="<?=the_permalink();?>">Читать</a>
							</div>
						</div>
					</div>
				<?php endwhile;?>	
			</div>

			<div class="row">
				<div class="col-12">
					<?php wp_pagenavi();?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>