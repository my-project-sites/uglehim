<?php 
	get_header(); 
	wp_reset_postdata();
?>

<div class="content">
	<h1>Статьи</h1>
	<div class="cardbox">
		<?php while(have_posts()): the_post(); ?>	
			<div class="cardbox__card cardbox__card_article">
				<?=get_the_post_thumbnail( $id, 'large', array('class' => 'cardbox__image_article') ); ?>
				<p class="cardbox__text"><?php custom_excerpt(strip_tags(get_the_content())); ?></p>
				<a class="cardbox__link" href="<?=the_permalink(); ?>">Выбрать</a>
			</div>
		<?php endwhile;?>	
	</div>
	
	<?php wp_pagenavi(); ?>
</div>

<?php get_footer(); ?>