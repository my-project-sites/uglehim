<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="social">
                <img src="<?=get_template_directory_uri();?>/assets/images/social/line.webp" alt="line">
                <a href="<?=do_shortcode('[userFacebook]');?>">
                    <img class="contactbox__image" src="<?=get_template_directory_uri();?>/assets/images/social/facebook.webp" alt="facebook"> 
                </a>
                
                <a href="<?=do_shortcode('[userInstagram]');?>">
                    <img class="contactbox__image" src="<?=get_template_directory_uri();?>/assets/images/social/instagram.webp" alt="instagram">
                </a>

                <a href="<?=do_shortcode('[userVK]');?>">
                    <img class="contactbox__image" src="<?=get_template_directory_uri();?>/assets/images/social/vk.webp" alt="twitter">
                </a>
                <img src="<?=get_template_directory_uri();?>/assets/images/social/line.webp" alt="line">
            </div>
        </div>
    </div>
</div>